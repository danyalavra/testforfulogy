import React from "react";
import ReactDOM from "react-dom";
import "./scss/main.scss";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";

import { createStore } from "redux";
import reducers from "./store/reducers";
import SideMenu from "./navigation/SideMenu";

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__()
);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route
        render={({ location, history }) => (
          <SideMenu location={location} history={history}></SideMenu>
        )}
      />
    </Router>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
