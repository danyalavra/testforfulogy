import React from "react";
import { tabs } from "../../languages/ru";
import Tab from "./Tab";
const Tabs = () => {
  return (
    <div className="tabs">
      <div className="container-fluid">
        {tabs.map((tab, index) => (
          //  Лучше использовать уникальный ID, но поскольку нижнее меню —
          //  статическое поле, мы можем позволить себе наглость использовать index
          <Tab tab={tab} key={index}></Tab>
        ))}
      </div>
    </div>
  );
};

export default Tabs;
