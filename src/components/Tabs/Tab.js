import React from "react";

const Tab = ({ tab: { title = "", className = "" } }) => {
  return (
    <div className={`tab ${className}`}>
      <span className="title">{title}</span>
    </div>
  );
};

export default Tab;
