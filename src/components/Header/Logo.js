import React from "react";
import logo from "../../img/logo.png";
import { logoAlt } from "../../languages/ru";
const Logo = () => {
  return (
    <div className={"logo"}>
      <img alt={logoAlt} src={logo}></img>
    </div>
  );
};

export default Logo;
