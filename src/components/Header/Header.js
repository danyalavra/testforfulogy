import React from "react";
import Logo from "./Logo";
import Cart from "./Cart";
import SideMenu from "../../navigation/SideMenu";

const Header = () => {
  return (
    <div className={"header"}>
      <div className={"container-fluid"}>
        <Logo></Logo>
        <Cart></Cart>
      </div>
    </div>
  );
};

export default Header;
