import React from "react";
import InfoButton from "./InfoButton";
import InfoText from "./InfoText";

const Info = () => {
  return (
    <div className="info">
      <InfoButton></InfoButton>
      <InfoText></InfoText>
    </div>
  );
};

export default Info;
