import React from "react";
import { connect } from "react-redux";
import { changeInfoState } from "../../../../store/actions";
const InfoButton = ({ showInfo }) => {
  return (
    <div className="info-button" onClick={showInfo}>
      <div className="icon-information"></div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    showInfo: () => {
      dispatch(changeInfoState(true));
    },
  };
};

export default connect(null, mapDispatchToProps)(InfoButton);
