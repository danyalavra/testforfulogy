import React from "react";
import { infoText } from "../../../../languages/ru";

const InfoText = () => {
  return <div className="text">{infoText}</div>;
};

export default InfoText;
