import React from "react";
import { connect } from "react-redux";
import { changeInfoState } from "../../../../store/actions";
const InfoOverlay = ({ isVisible, hideInfoOverlay }) => {
  return !isVisible ? null : (
    <div className="info-overlay overlay d-flex flex-column">
      <div className="back" onClick={hideInfoOverlay}>
        <div className="gray-bg caret"></div>
        <div className="gray">Вернуться</div>
      </div>
      <p className="mt-3">
        {
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque quis ullamcorper orci. Quisque nec ipsum non diam eleifend posuere. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum varius tincidunt ipsum, nec pellentesque sapien scelerisque non. Cras fringilla felis et orci pulvinar, at malesuada velit condimentum. Sed iaculis nibh turpis, non ultrices elit convallis at. Morbi blandit, magna at hendrerit mattis, dui purus euismod turpis, at consectetur leo sem nec sem. Ut scelerisque urna sit amet risus imperdiet venenatis. Cras ipsum est, scelerisque vitae justo mattis, luctus egestas orci. Donec porttitor lectus non vulputate pretium. Integer eget iaculis elit. Duis ipsum magna, euismod nec ultrices ac, pulvinar at sapien. Cras elementum non elit pellentesque blandit. Nunc eget nisi semper, scelerisque lacus sed, sagittis elit. Vivamus non sapien mollis, tincidunt felis in, porttitor quam."
        }
      </p>
      <p className="mt-3">
        {
          "Vestibulum varius tincidunt ipsum, nec pellentesque sapien scelerisque non. Cras fringilla felis et orci pulvinar, at malesuada velit condimentum. Sed iaculis nibh turpis, non ultrices elit convallis at. Morbi blandit, magna at hendrerit mattis, dui purus euismod turpis, at consectetur leo sem nec sem. Ut scelerisque urna sit amet risus imperdiet venenatis. Cras ipsum est, scelerisque vitae justo mattis, luctus egestas orci. Donec porttitor lectus non vulputate pretium. Integer eget iaculis elit. Duis ipsum magna, euismod nec ultrices ac, pulvinar at sapien. Cras elementum non elit pellentesque blandit. Nunc eget nisi semper, scelerisque lacus sed, sagittis elit. Vivamus non sapien mollis, tincidunt felis in, porttitor quam."
        }
      </p>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { info } = state;
  return {
    isVisible: info,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    hideInfoOverlay: () => {
      dispatch(changeInfoState(false));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InfoOverlay);
