import React from "react";
import { connect } from "react-redux";
import { changeCategory } from "../../../../store/actions";
import CheckBox from "./CheckBox";
const Card = ({
  card: { categoryId = "", name = "", src = "" },
  isActive = false,
  chooseCategory = () => {},
}) => {
  return (
    <div className="card col-4" onClick={() => chooseCategory(categoryId)}>
      <div className="body">
        <div className="img-wrapper" style={{ backgroundImage: `url(${src})` }}>
          {/* <img src={src} className="img-fluid" alt={noImageText}></img> */}
          <div className="layout">
          {isActive ? <CheckBox /> : null}
            <div className="name">{name}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    chooseCategory: (categoryId) => {
      dispatch(changeCategory(categoryId));
    },
  };
};

export default connect(null, mapDispatchToProps)(Card);
