import React, { useState } from "react";
import Card from "./Card";
import { connect } from "react-redux";

const Cards = ({ category }) => {
  const [cards] = useState([
    {
      categoryId: 1,
      name: "Теплый",
      src:
        "https://www.imago-images.com/imagoextern/bilder/stockfotos/en/imago-images-aerial-photography.jpg",
    },
    {
      categoryId: 2,
      name: "Дневной",
      src:
        "https://www.imago-images.com/imagoextern/bilder/stockfotos/en/imago-images-aerial-photography.jpg",
    },
    {
      categoryId: 3,
      name: "Холодный",
      src:
        "https://www.imago-images.com/imagoextern/bilder/stockfotos/en/imago-images-aerial-photography.jpg",
    },
  ]);
  return (
    <div className="cards row d-flex justify-content-center">
      {cards.map((card) => {
        const { categoryId} =card
        return <Card key={categoryId} isActive={categoryId == category} card={card}></Card>;
      })}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { category } = state;
  return {
    category,
  };
};

export default connect(mapStateToProps, null)(Cards);
