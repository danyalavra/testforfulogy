import React from "react";

const CheckBox = () => {
  return (
    <div className="check-box">
      <div className="check"></div>
    </div>
  );
};

export default CheckBox;
