import React from "react";
import Attributes from "./Attributes/Attributes";
import Info from "./Info/Info";
import Cards from "./Cards/Cards";
const RightBlock = () => {
  return (
    <div className="right-block col-12 col-md-6">
      <Attributes></Attributes>
      <Info></Info>
      <Cards></Cards>
    </div>
  );
};

export default RightBlock;
