import React from "react";
import { connect } from "react-redux";
import ClickOutside from "../../../Support/ClickOutside";
import { changeOverlayState } from "../../../../store/actions";
import { changeClassQuestion, yes, no } from "../../../../languages/ru";
import Button from "../../../UI-kit/Button";

const AttributeOverlay = ({ isVisible, hideOverlay }) => {
  if (!isVisible) {
    return null;
  }

  return (
    <div onClick={hideOverlay} className="overlay">
      <div className="card">
        <div className="title">{changeClassQuestion}</div>

        <div className="d-flex justify-center mt-2">
          <Button
            onClick={hideOverlay}
            className={"pink-bg shadow mx-2"}
            text={yes}
          ></Button>
          <Button
            onClick={hideOverlay}
            className={"accent-bg shadow mx-2"}
            text={no}
          ></Button>
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state, ownProps) => {
  const { overlay } = state;
  return {
    isVisible: overlay,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    hideOverlay: () => {
      dispatch(changeOverlayState(false));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AttributeOverlay);
