import React from "react";
import { connect } from "react-redux";
import { changeOverlayState } from "../../../../store/actions";


const AttributesClassButton = ({ text, showOverlay }) => {
  return (
    <button className="attribute-class-button" onClick={showOverlay}>
      {text}
    </button>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    showOverlay: () => {
      dispatch(changeOverlayState(true));
    },
  };
};

export default connect(null, mapDispatchToProps)(AttributesClassButton);
