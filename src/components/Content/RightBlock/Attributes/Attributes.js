import React from "react";
import AttributesClassButton from "./AttributesClassButton";
import Attribute from "./Attribute";
import AttributeOverlay from "./AttributeOverlay";

const Attributes = () => {
  const attributesList = [
    { attr: "Класс:", content: <AttributesClassButton text={"Standart"} /> },
    { attr: "Потребляемая мощность:", content: "59 Вт" },
    {
      attr: "Сила света ",
      content: "3459 Люмен = 7,5 ламп накаливания по 40 Вт.",
    },
    { attr: "Гарантия:", content: "3 года" },
    { attr: "Монтаж:", content: "Да" },
    { attr: "Итого:", content: "2549 рублей" },
  ];

  return (
    <div className="attributes">
      {attributesList.map((item, index) => (
        <Attribute key={index} item={item} />
      ))}
      <AttributeOverlay></AttributeOverlay>
    </div>
  );
};

export default Attributes;
