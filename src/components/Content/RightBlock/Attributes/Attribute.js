import React from "react";
const Attribute = ({ item: { attr = "", content = null } }) => {
  return (
    <div className="item d-flex">
      <div className={"attr col-6 text-smaller"}>{attr}</div>
      <div className={"d-flex align-items-end col-6"}>{content}</div>
    </div>
  );
};

export default Attribute;
