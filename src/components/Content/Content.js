import React from "react";
import LeftBlock from "./LeftBlock/LeftBlock";
import RightBlock from "./RightBlock/RightBlock";
import InfoOverlay from "./RightBlock/Info/InfoOverlay";
const Content = () => {
  return (
    <div className="content row container-fluid">
      <LeftBlock></LeftBlock>
      <RightBlock></RightBlock>
      <InfoOverlay></InfoOverlay>
    </div>
  );
};

export default Content;
