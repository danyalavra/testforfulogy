import React from "react";
import Slider from "./Slider/Slider";
const LeftBlock = () => {
  return (
    <div className="left-block col-12 col-md-6">
      <Slider></Slider>
    </div>
  );
};

export default LeftBlock;
