import React from "react";
import Bullet from "./Bullet";

const Bullets = ({ count, current, setIndex }) => {
  return (
    <div className="bullets">
      {Array(count)
        .fill(0)
        .map((v, index) => {
          return (
            <Bullet
              onClick={() => setIndex(index)}
              isActive={index == current}
            ></Bullet>
          );
        })}
    </div>
  );
};

export default Bullets;
