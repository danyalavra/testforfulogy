import React from "react";

const Bullet = ({ isActive, onClick }) => {
  return (
    <div
      onClick={onClick}
      className={`bullet ${isActive ? "active" : ""}`}
    ></div>
  );
};

export default Bullet;
