import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Bullets from "./Bullets";

const Slider = ({ images: { src = [] } }) => {
  const [currentImageIndex, setIndex] = useState(0);

  return (
    <div className="slider">
      <div
        className="image-wrapper"
        style={{ backgroundImage: `url(${src[currentImageIndex]})` }}
      >
        <Bullets
          current={currentImageIndex}
          count={src.length}
          setIndex={setIndex}
        ></Bullets>
      </div>
    </div>
  );
};
const mapStateToProps = (state, ownProps) => {
  const { images = [], category } = state;
  return {
    images: images.find((i) => i.category == category),
  };
};

export default connect(mapStateToProps, null)(Slider);
