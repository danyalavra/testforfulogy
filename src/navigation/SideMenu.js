import React, { useState } from "react";
import ClickOutside from "../components/Support/ClickOutside";

import SideNav, {
  Toggle,
  Nav,
  NavItem,
  NavText,
} from "@trendmicro/react-sidenav";
import "@trendmicro/react-sidenav/dist/react-sidenav.css";
import { Route, Switch, Link } from "react-router-dom";
import App from "../App";

const navLinks = [
  { to: "", text: "Обучающее видео" },
  {
    to: "purchase",
    text: "Оформление заказа",
  },
  {
    to: "payment",
    text: "Оплата",
  },
  { to: "delivery", text: "Доставка" },
  { to: "guarantee", text: "Гарантия" },
  { to: "return", text: "Возврат" },
  { to: "contacts", text: "Контакты" },
  { to: "partners", text: "Партнерам" },
];

const SideMenu = ({ location, history }) => {
  const [expanded, setExpanded] = useState(false);
  return (
    <>
      <ClickOutside
        onClickOutside={() => {
          setExpanded(false);
        }}
      >
        <SideNav
          className
          onSelect={(selected) => {
            const to = "/" + selected;
            if (location.pathname !== to) {
              history.push(to);
            }
            setExpanded(false);
          }}
          expanded={expanded}
          onToggle={(exp) => {
            setExpanded(exp);
          }}
        >
          <Toggle className="side-menu-toggle" />
          <Nav defaultSelected={"/"}>
            {navLinks.map(({ to, text }, index) => {
              return (
                <NavItem
                  navitemClassName={"nav-item"}
                  subnavClassName="sub-nav-item"
                  key={index}
                  eventKey={to}
                >
                  <NavText>{text}</NavText>
                </NavItem>
              );
            })}
          </Nav>
        </SideNav>
      </ClickOutside>
      <main>
        <Switch>
          <Route path="/" exact component={App} />
          <Route
            component={() => (
              <div>
                Ошибка, нет такого экрана! <Link to="/">Назад</Link>
              </div>
            )}
          />
        </Switch>
      </main>
    </>
  );
};

export default SideMenu;
