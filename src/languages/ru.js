// Здесь, потому что тестовый вариант. В идеале нижние переключители делаются на основе роутинга
export const tabs = [
  { title: "Вариант кухни", className: "saved" },
  { title: "Размеры", className: "saved" },
  { title: "Сенсон", className: "saved" },
  { title: "Питающий кабель", className: "saved" },
  { title: "Блок питания", className: "saved" },
  { title: "Цвет свечения", className: "current-page" },
  { title: "Монтаж", className: "next-page" },
  { title: "Корзина", className: "next-page" },
];

export const logoAlt = "Здесь должен быть логотип";
export const infoText = "Выберите цвет свечения";
export const noImageText = "Нет картинки";
export const yes = "Да";
export const no = "Нет";

export const changeClassQuestion =
  "Вы действительно хотите сменить класс светильника?";
