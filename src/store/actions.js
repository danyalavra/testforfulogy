import T from "./types";

export const changeCategory = (categoryId) => ({
  type: T.CHANGE_CATEGORY,
  categoryId,
});
export const changeOverlayState = (isVisible) => ({
  type: T.CHANGE_OVERLAY,
  payload: isVisible,
});
export const changeInfoState = (isVisible) => ({
  type: T.CHANGE_INFO,
  payload: isVisible,
});
