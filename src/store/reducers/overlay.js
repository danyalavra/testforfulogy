import T from "../types";

let initialState = false;

export default function completions(state = initialState, action) {
  switch (action.type) {
    case T.CHANGE_OVERLAY:
      return action.payload;
    default:
      return state;
  }
}
