import T from "../types";

let initialState = 1;

export default function completions(state = initialState, action) {
  switch (action.type) {
    case T.CHANGE_CATEGORY:
      const { categoryId } = action;
      return categoryId;
    default:
      return state;
  }
}
