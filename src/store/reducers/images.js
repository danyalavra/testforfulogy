let initialState = [
  {
    category: "1",
    src: [
      "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg",
      "https://static.toiimg.com/thumb/72975551.cms?width=680&height=512&imgsize=881753",
      "https://www.imago-images.com/imagoextern/bilder/stockfotos/en/imago-images-aerial-photography.jpg",
    ],
  },
  {
    category: "2",
    src: [
      "https://cdn.pixabay.com/photo/2015/02/24/15/41/dog-647528__340.jpg",
      "https://www.gettyimages.com/gi-resources/images/500px/983794168.jpg",
      "https://dynaimage.cdn.cnn.com/cnn/c_fill,g_auto,w_1200,h_675,ar_16:9/https%3A%2F%2Fcdn.cnn.com%2Fcnnnext%2Fdam%2Fassets%2F191120053137-03-milky-way-images-australia.jpg",
    ],
  },
  {
    category: "3",
    src: [
      "https://quotiyapa.com/wp-content/uploads/2019/10/cute-couple-image-1024x682.jpg",
      "https://killerattitudestatus.in/wp-content/uploads/2019/12/gud-night-images.jpg",
      "https://i.pinimg.com/originals/ca/76/0b/ca760b70976b52578da88e06973af542.jpg",
    ],
  },
];

export default function images(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
