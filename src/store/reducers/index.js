import { combineReducers } from "redux";

import images from "./images";
import category from "./category";
import overlay from "./overlay";
import info from "./info";
export default combineReducers({
  info,
  overlay,
  category,
  images,
});
