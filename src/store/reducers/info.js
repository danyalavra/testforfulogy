import T from "../types";

let initialState = false;

export default function info(state = initialState, action) {
  switch (action.type) {
    case T.CHANGE_INFO:
      return action.payload;
    default:
      return state;
  }
}
