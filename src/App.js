import React from "react";
import Tabs from "./components/Tabs/Tabs";
import Header from "./components/Header/Header";
import Content from "./components/Content/Content";
import SideMenu from "./navigation/SideMenu";

function App() {
  return (
    <>
      <Header></Header>
      <Content></Content>
      <Tabs></Tabs>
    </>
  );
}

export default App;
